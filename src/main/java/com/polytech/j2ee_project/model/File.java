
package com.polytech.j2ee_project.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.primefaces.model.TreeNode;


@Entity
@Table(name="file")
public class File implements Serializable,FileManagerElement {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;
    
    @Column(name="file")
    private String name;
    @Column(name="date_inserted")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name="date_updated")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name="date_file")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date fileDate;
    @Column(name="remark")
    private String remark;
    @Column(name="file_type")
    private String type;
    @Column(name="file_size")
    private long size;
    @Column(name="file_original_name")
    private String originalFilename;
    @Column(name="path")
    private String path;
    
    
    @ManyToOne
    @JoinColumn(name = "fk_folder_file", referencedColumnName = "id_folder")
    private Folder folder;
    
    @Transient
    private final static DecimalFormat format = new DecimalFormat("#0.00");
            
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }
    
    @PrePersist
    private void automaticDateInsert(){
        Date date = new Date();
        if(this.getId() == null){
            this.setInsertDate(date);
        }
        this.setUpdateDate(date);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof File)) {
            return false;
        }
        File other = (File) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString(){
        return getName();
    }

    @Override
    public String getIconPath() {
        return "images/file.png";
     }

    @Override
    public String getElementName() {
       return getName();
    }

    @Override
    public String getElementSize() {
        return String.valueOf(format.format(getSize()/1000000.)) + " Mo";
    }
    
    
}
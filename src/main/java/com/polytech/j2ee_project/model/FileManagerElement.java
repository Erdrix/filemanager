package com.polytech.j2ee_project.model;

import org.primefaces.model.TreeNode;

/**
 *
 * @author Romain
 */
public interface FileManagerElement{
    public Long getId();
    public String getIconPath();
    public String getElementName();
    public String getElementSize();
}

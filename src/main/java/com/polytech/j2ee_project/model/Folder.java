package com.polytech.j2ee_project.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.primefaces.model.TreeNode;


@Entity
@Table(name="folder")
@NamedQuery(name="Folder.findRoot", query="select f from Folder f where f.parentFolder IS NULL ")

public class Folder implements Serializable, Comparable<Folder>, FileManagerElement {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_folder")
    private Long id;
    
    @Column(name="fname")
    private String name;
    @Column(name="date_inserted")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date insertDate;
    @Column(name="date_updated")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updateDate;
    
    @Transient
    private TreeNode node;
    @Transient
    private final static DecimalFormat format = new DecimalFormat("#0.00");
    public TreeNode getNode() {
        return node;
    }

    public void setNode(TreeNode node) {
        this.node = node;
    }
    
    @ManyToOne
    @JoinColumn(name = "fk_folder_id_parent", referencedColumnName = "id_folder")
    private Folder parentFolder;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentFolder", orphanRemoval =  true)
    private List<Folder> childFolders;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "folder")
    private List<File> files;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public List<Folder> getChildFolders() {
        return childFolders;
    }

    public Folder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(Folder parentFolder) {
        this.parentFolder = parentFolder;
    }   
    
    public void addChildFolder(Folder child){
        this.childFolders.add(child);
    }
    
    public void setChildFolders(List<Folder> childFolders) {
        this.childFolders = childFolders;
    }

    public List<File> getFile() {
        return files;
    }

    public void setFile(List<File> files) {
        this.files = files;
    }
    
    public void addFile(File file){
        this.files.add(file);
    }

    
    @PrePersist
    private void automaticDateInsert(){
        Date date = new Date();
        if(this.getId() == null){
            this.setInsertDate(date);
        }
        this.setUpdateDate(date);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Folder)) {
            return false;
        }
        Folder other = (Folder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString(){
        return getName();
    }

    @Override
    public int compareTo(Folder o) {
        return (this.name.compareTo(o.getName()));
    }
    @Override
    public String getIconPath() {
        return "images/folder_1.png";
     }

    @Override
    public String getElementName() {
      return getName();
    }

    public long getSize(){
        long size=0;
        for(File file:files){
            size+=file.getSize();
        }
        return size;
    } 
            
    @Override
    public String getElementSize() {
        float size=0;
        for(File file:files){
            size+=file.getSize()/1000000.;
        }
        for(Folder folder:childFolders){
            size+=folder.getSize()/1000000.;
        }
        return String.valueOf(format.format(size)) + " Mo";
    }
}

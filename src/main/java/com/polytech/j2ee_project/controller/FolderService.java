package com.polytech.j2ee_project.controller;

import com.polytech.j2ee_project.ejb.FileEJB;
import com.polytech.j2ee_project.ejb.FolderEJB;
import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.Folder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WorkSpace_Erdrixx
 */
@ManagedBean(name ="folderService")
@ApplicationScoped
public class FolderService {
    @EJB
    private FolderEJB folderEJB;
    
    @EJB 
    private FileEJB fileEJB;
    
    
    public TreeNode init(){       
        Folder froot = folderEJB.getRootFolder();
           
        TreeNode root = new DefaultTreeNode("root", null);
        TreeNode realRoot = new DefaultTreeNode(froot, root);
        froot.setNode(realRoot);

        for (Folder child : froot.getChildFolders()) {
            TreeNode tnChild = new DefaultTreeNode(child, realRoot);
            tnChild.setParent(realRoot);
            child.setNode(tnChild);
            buildTreeRecursively(tnChild);
        }
        return root;
    }
    
    private void buildTreeRecursively(TreeNode currentNode) {
        Folder folder = (Folder) (currentNode.getData());
        for (Folder child : folder.getChildFolders()) {
            TreeNode tnChild = new DefaultTreeNode(child, currentNode);
            tnChild.setParent(currentNode);
            child.setNode(tnChild);
            buildTreeRecursively(tnChild);
        }
    }
    
    public void doUpdate(TreeNode selectedNode, String newNodeName) {
        if (selectedNode != null && !checkName(selectedNode.getParent(), newNodeName)) {
            ((Folder)selectedNode.getData()).setName(newNodeName);
            renameFolder(selectedNode, newNodeName);
            folderEJB.modifyFolder((Folder) selectedNode.getData());
        }
    }
    
    
    public void doDelete(TreeNode selectedNode) {
        if (selectedNode != null) {
            Folder folder = (Folder) selectedNode.getData();
            folder.getParentFolder().getChildFolders().remove(folder);
            folder.setParentFolder(null);
            for(Folder child:folder.getChildFolders()){
               deleteFiles(child); 
            }
            folder = folderEJB.refreshFolder(folder.getId());
            List<File> tmp = new ArrayList<>(folder.getFile());
            for(File file:tmp){
                folder.getFile().remove(file);
                fileEJB.deleteFile(file);
            }
            folderEJB.deleteFolder(folder);
            
            selectedNode.getChildren().clear();
            selectedNode.getParent().getChildren().remove(selectedNode);
            selectedNode.setParent(null);
            selectedNode = null;
        }
    }
    
    public void deleteFiles(Folder f){
        for(Folder child:f.getChildFolders()){
               deleteFiles(child); 
            }
       
        List<File> tmp = new ArrayList<>(f.getFile());
            for(File file:tmp){
                f.getFile().remove(file);
                fileEJB.deleteFile(file);
            }
    }
    public void doCreate(TreeNode selectedNode, String newFolderName) {
        if (selectedNode != null && !checkName(selectedNode, newFolderName)) {
            Folder newFolder = folderEJB.addNewFolder(newFolderName, (Folder) selectedNode.getData());
            TreeNode tnNewNode = new DefaultTreeNode(newFolder, selectedNode);
            newFolder.setNode(tnNewNode);
            makeFolder(selectedNode, newFolderName);
            selectedNode.setExpanded(true);
        }
    }
    
   
    
    // Permet de vérifier qu'aucun noeud ne possède le même nom au même niveau.
    public boolean checkName(TreeNode myNode, String name){
       List<TreeNode> childrens = myNode.getChildren();
       
       for( TreeNode child : childrens){
           if(child.getData().toString().equals(name))
               return true;
       }
       
       List<File> files = ((Folder) myNode.getData()).getFile();
       for( File child : files){
           if(child.getName().toString().equals(name))
               return true;
       }
       return false;
    }
    
    // Permet de récupérer le chémin du dossier à partir du noeud.
    public String createPath(TreeNode myNode){
        // Racine des dossier
        String filePath = System.getProperty("user.home") + java.io.File.separator + "FileManager";

        ArrayList<String> parents = new ArrayList<>();
        TreeNode currentNode = myNode;
        
        // Trouve tous les noeuds du chemin vers le dossier.
        while(currentNode != null){
            String name = currentNode.getData().toString();
            parents.add(name);
            currentNode = currentNode.getParent();
        }
        
        // Construit le chemin.
        for(int j = parents.size()-3; j>=0; j--)
            filePath += java.io.File.separator + parents.get(j) ;

        return filePath;           
    }
    
    // Permet de renommer un dossier en un autre
    public boolean renameFolder( TreeNode myNode, String nName){
        String path = createPath(myNode.getParent()); 
        java.io.File newName = new java.io.File(path+java.io.File.separator+nName);
        java.io.File oldName = new java.io.File(path+java.io.File.separator+myNode.getData().toString());
        oldName.mkdirs();
        return oldName.renameTo(newName);
    }
    
    // Permet de supprimer un dossier entier.
    public boolean deleteFolder(TreeNode myNode){
        String path = createPath(myNode);
        java.io.File deletePath = new java.io.File(path);

        return deletePath.delete();
    }
    public boolean deleteFile(File f){
        java.io.File deletePath = new java.io.File(f.getPath());
        return deletePath.delete();
    }
    // Permet de créer le dossier si jamais il n'existe pas déjà.
    public boolean makeFolder(TreeNode myNode, String name){
        String path = createPath(myNode);
      
        java.io.File file = new java.io.File(path+java.io.File.separator+name);
        return file.mkdirs();
    }
}

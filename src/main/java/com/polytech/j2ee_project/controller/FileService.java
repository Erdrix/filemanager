/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polytech.j2ee_project.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.polytech.j2ee_project.ejb.FileEJB;
import com.polytech.j2ee_project.ejb.FolderEJB;
import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.Folder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import javax.activation.MimetypesFileTypeMap;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author WorkSpace_Erdrixx
 */
@ManagedBean(name ="fileService")
@ApplicationScoped
public class FileService {
    
    @EJB
    private FolderEJB folderEJB;
    
    @EJB 
    private FileEJB fileEJB;
    
    @ManagedProperty("#{folderService}")
    private FolderService service;
    
    public void addFile(UploadedFile uploadedFile, TreeNode folder) throws IOException{
        
        String name = uploadedFile.getFileName();
        String path = service.createPath(folder)+java.io.File.separator+name;
        String mimeType = uploadedFile.getContentType();
        Date dateFile = new Date();
       
        fileEJB.addNewFile(name, "", path, name,mimeType,dateFile, (Folder) folder.getData()); 
    }
    public ByteArrayInputStream txtToPdf(java.io.File file){
        Document document = new Document();
        ByteArrayOutputStream out = null;
        try {
            out = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, out);
            document.open();
          
            FileInputStream fis = new FileInputStream(file);
 
            //Construct BufferedReader from InputStreamReader
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));

            String line = null;
            while ((line = br.readLine()) != null) {
                    document.add(new Paragraph(line));
            }
            br.close();     
          document.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    }
    public ByteArrayInputStream imgToPdf(java.io.File file){
        Document document = new Document();
        ByteArrayOutputStream out = null;
        try {
          out = new ByteArrayOutputStream();
          PdfWriter.getInstance(document, out);
          document.open();
          Image img = Image.getInstance(file.getAbsolutePath());
          img.scaleToFit(500, 10000);
          document.add(img);
          document.close();
        }
        catch (Exception e) {
          e.printStackTrace();
        }
        return new ByteArrayInputStream(out.toByteArray());
    } 
    
    public void deleteFile(File f){
        java.io.File deletePath = new java.io.File(f.getPath());
        deletePath.delete();
        fileEJB.deleteFile(f);
    }
    
     
    public FolderEJB getFolderEJB() {
        return folderEJB;
    }

    public void setFolderEJB(FolderEJB folderEJB) {
        this.folderEJB = folderEJB;
    }

    public FileEJB getFileEJB() {
        return fileEJB;
    }

    public void setFileEJB(FileEJB fileEJB) {
        this.fileEJB = fileEJB;
    }

    public FolderService getService() {
        return service;
    }

    public void setService(FolderService service) {
        this.service = service;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.polytech.j2ee_project.controller;


import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.FileManagerElement;
import com.polytech.j2ee_project.model.Folder;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Erdrixx
 */

@ManagedBean(name="folderController")
@SessionScoped
public class FolderController {
    
    // ATTRIBUTS
    private TreeNode root;
    private TreeNode selectedNode;
    private ArrayList<TreeNode> nodeList;
    private String newNodeName;
    private String path ="";
    private List<FileManagerElement> content;
    private List<FileManagerElement> selectedElements;
    private FileManagerElement selectedElement;
    private String absolutePath="";
    StreamedContent sContent;
    String contentType="";
    java.io.File file;
    DefaultStreamedContent download;
    String downloadName = "";
    String currentView ="Board";

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
 
    @ManagedProperty("#{fileService}")
    private FileService fileService;
    @ManagedProperty("#{folderService}")
    private FolderService service;
    
    @PostConstruct
    public void init() {
        root = service.init(); 
        root.setExpanded(true);
        root.setSelected(true);
        content = new ArrayList<>();
        selectedElements = new ArrayList<>();
    }
    
    private static final long serialVersionUID = 1L;
    private UploadedFile resume;

  
    //This below code is for file upload with advanced mode.
    public void uploadFile(FileUploadEvent e) throws IOException{
        UploadedFile uploadedFile=e.getFile();
        String filePath = System.getProperty("user.home") + java.io.File.separator + "FileManager" +java.io.File.separator;
        if(selectedNode !=null){
            filePath = service.createPath(selectedNode)+java.io.File.separator;
        }
        if(!service.checkName(selectedNode, uploadedFile.getFileName())){
            byte[] bytes=null;
            if (null!=uploadedFile) {
                
                
                // Récupère le contenu du fichier.
                bytes = uploadedFile.getContents();
                
                // Récupère le nom du fichier.
                String filename = uploadedFile.getFileName();
                
                // Créer le chemin dans l'arborescence si ça n'est pas déjà fait.
                java.io.File dir = new java.io.File(filePath);              
                dir.mkdirs();
                
                // Créer le fichier au bon emplacement.
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new  java.io.File(filePath+filename)));
                stream.write(bytes);
                stream.close();
                fileService.addFile(uploadedFile, selectedNode);
                FacesContext.getCurrentInstance().addMessage("messages",new FacesMessage(FacesMessage.SEVERITY_INFO,"Your File (File Name "+ uploadedFile.getFileName()+ " with size "+ uploadedFile.getSize()+ ")  Uploaded Successfully", ""));
            } 
        }else{
            FacesContext.getCurrentInstance().addMessage("messages",new FacesMessage(FacesMessage.SEVERITY_WARN,"Your File (File Name "+ uploadedFile.getFileName()+ " with size "+ uploadedFile.getSize()+ ") can't be load, delete the file with the same name before", ""));
    
        }
        content.clear();
        content.addAll(((Folder)selectedNode.getData()).getChildFolders());
        content.addAll(((Folder)selectedNode.getData()).getFile());
        // Affiche un message signifiant que l'opération s'est bien déroulée.
        }
   
    /**
     * Create a new node with the selected node like parent
     */
    public void doCreate() {
        service.doCreate(selectedNode, newNodeName);
        content.clear();
        content.addAll(((Folder)selectedNode.getData()).getChildFolders());
        content.addAll(((Folder)selectedNode.getData()).getFile());
    }

    /**
     * Update name node
     */
    public void doUpdate() {
        service.doUpdate(selectedNode, newNodeName);
        path = service.createPath(selectedNode);
        content.clear();
        content.addAll(((Folder)selectedNode.getData()).getChildFolders());
        content.addAll(((Folder)selectedNode.getData()).getFile());
    }
    
    public String goBoard(){
        return "board.xhtml";
    }
    
    /**
     * Delete node selected
     */
    public void doDelete() {
        TreeNode parent = selectedNode.getParent();
        if (!((Folder)selectedNode.getData()).getName().equals("root")){
            deleteAllSubElement((Folder) selectedNode.getData());
            ((Folder)parent.getData()).getChildFolders().remove(selectedNode.getData());
            service.doDelete(selectedNode);

            selectedNode = parent;
            path = service.createPath(selectedNode);
            selectedNode.setSelected(true);
            content.clear();
            content.addAll(((Folder)selectedNode.getData()).getChildFolders());
            content.addAll(((Folder)selectedNode.getData()).getFile());
        }
        else
             FacesContext.getCurrentInstance().addMessage("messages",new FacesMessage(FacesMessage.SEVERITY_WARN,"You can't remove the root folder",""));
    
    }
    public void deleteAllSubElement(Folder f){
        List<Folder> childs = f.getChildFolders();
        for(Folder folder:childs){
            deleteAllSubElement(folder);
        }
        for(File file:f.getFile()){
            java.io.File deletePath = new java.io.File(file.getPath());
            deletePath.delete();
        }
        String path = service.createPath(f.getNode()) +java.io.File.separator;
        java.io.File deleteFolder = new java.io.File(path);
        deleteFolder.delete();
        
    }
    /**
     * Insert a file.
     * @return 
     */
    public void doCreateFile() throws IOException
    {
        if(selectedNode!= null){
            currentView = "UploadFile";
            FacesContext.getCurrentInstance().getExternalContext().redirect("addFile.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }    
    }
    
    public void deleteElements(){
        for(FileManagerElement element:selectedElements){
            if(element instanceof Folder){
                
                selectedNode = ((Folder)element).getNode();
                doDelete();
            }
            if(element instanceof File){
                
                File f = ((File)element);
                ((Folder)selectedNode.getData()).getFile().remove(f);
                fileService.deleteFile(f);
                 content.clear();
                 content.addAll(((Folder)selectedNode.getData()).getChildFolders());
                 content.addAll(((Folder)selectedNode.getData()).getFile());
            }
        }
    }
    public void onNodeSelect(NodeSelectEvent event) {
        
        selectedNode = (event.getTreeNode());
        path = service.createPath(selectedNode);
        content.clear();
        content.addAll(((Folder)selectedNode.getData()).getChildFolders());
        content.addAll(((Folder)selectedNode.getData()).getFile());
    }
    
    public void onElementSelect(SelectEvent e){
        try {
            String redirect = onElementSelect((FileManagerElement) e.getObject());
            if(redirect != null){
                FacesContext.getCurrentInstance().getExternalContext().redirect(redirect);
                FacesContext.getCurrentInstance().responseComplete();
            }
        } catch (IOException ex) {
            Logger.getLogger(FolderController.class.getName()).log(Level.SEVERE, null, ex);
        }
           
    }
    
    public String onElementSelect(FileManagerElement element) throws FileNotFoundException{
        
        this.selectedElement = element;
        if(element instanceof Folder){
            path = service.createPath(((Folder) element).getNode());
            this.selectedNode = ((Folder) element).getNode();
            ((Folder) element).getNode().getParent().setExpanded(true);
            ((Folder) element).getNode().getParent().setSelected(false);
            ((Folder) element).getNode().setSelected(true);
            content.clear();
            content.addAll(((Folder)selectedNode.getData()).getChildFolders());
            content.addAll(((Folder)selectedNode.getData()).getFile());
        }
        
        if(element instanceof File){
            //Action for FileElement
            file = new java.io.File(service.createPath(selectedNode)+"/"+((File)element).getName());
            
            absolutePath = file.getAbsolutePath();
            contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(absolutePath);
            downloadName = file.getName();
             currentView = "ReaderFile";
            return "reader.xhtml";   
        }
        return null;
    }
    
    public void prepDownload() throws Exception {
        contentType = FacesContext.getCurrentInstance().getExternalContext().getMimeType(absolutePath);
    }
 
    
    public String getIcon(){
        return "images/folder_1.png"; 
    }
    

    public TreeNode getRoot(){return root;}
    public ArrayList<TreeNode> getNodeList(){return nodeList;}
    public TreeNode getSelectedNode() {return selectedNode;}
    public void setSelectedNode(TreeNode selectedNode) {this.selectedNode = selectedNode;}
    public String getNewNodeName(){return newNodeName;}
    public void setNewNodeName(String newNodeName){this.newNodeName = newNodeName;} 
    public UploadedFile getResume() { return resume;}
    public void setResume(UploadedFile resume) {this.resume = resume;}
    public void setService(FolderService service) {this.service = service;}
    public FolderService getService(){return service;}
    public void setContent(List<FileManagerElement> l){content = l;};
    public List<FileManagerElement> getContent(){return content;};
     public void setSelectedElement(FileManagerElement e){selectedElement = e;};
    public FileManagerElement getSelectedElement(){return selectedElement;};

    public StreamedContent getsContent() throws FileNotFoundException {
        
        if(contentType.equals("application/pdf"))
            return new DefaultStreamedContent(new FileInputStream(absolutePath), contentType);
        else if(contentType.equals("image/png") ||contentType.equals("image/jpeg") || contentType.equals("image/gif"))
        {
            
            return new DefaultStreamedContent(fileService.imgToPdf(new java.io.File(absolutePath)), "application/pdf");
        }
        else
            return new DefaultStreamedContent(fileService.txtToPdf(new java.io.File(absolutePath)), "application/pdf");
    }

    public void setsContent(StreamedContent sContent) {
        this.sContent = sContent;
    }
    

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }
    public void setSelectedElements(List<FileManagerElement> l){selectedElements = l;};
    public List<FileManagerElement> getSelectedElements(){return selectedElements;};
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileService getFileService() {
        return fileService;
    }

    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public DefaultStreamedContent getDownload() throws FileNotFoundException {
        
        return new DefaultStreamedContent(new FileInputStream(absolutePath), contentType, downloadName);
    }

    public void setDownload(DefaultStreamedContent download) {
        this.download = download;
    }

    public String getCurrentView() {
        return currentView;
    }

    public void setCurrentView(String currentView) {
        this.currentView = currentView;
    }
    
    
}

    

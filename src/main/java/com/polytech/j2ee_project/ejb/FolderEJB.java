package com.polytech.j2ee_project.ejb;

import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.Folder;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
public class FolderEJB {

    @PersistenceContext(unitName = "FileManagerPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    
 
    public Folder getRootFolder() {
        Folder root;
        try{
            Query query = em.createNamedQuery("Folder.findRoot");
            root = (Folder) query.getSingleResult();
        }catch(NoResultException e){
            root = new Folder();
            root.setName("root");
            em.persist(root);          
        }
        return root;
    }
    
    public Folder addNewFolder(String FolderName, Folder parent) {

        Folder newFolder = new Folder();
        newFolder.setName(FolderName);
        newFolder.setParentFolder(parent);
        parent.addChildFolder(newFolder);
        em.persist(newFolder);
        em.merge(parent);
        
        return newFolder;
    }
    
    public void modifyFolder(Folder folder) {
        em.merge(folder);
    }

    public Folder refreshFolder(long idFolder){
        return em.find(Folder.class, idFolder);
    }
    public void deleteFolder(Folder folder) {
        Folder toRemoved = em.merge(folder);
        em.remove(toRemoved);
    }
}
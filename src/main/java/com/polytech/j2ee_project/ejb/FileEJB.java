package com.polytech.j2ee_project.ejb;


import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.Folder;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cedric
 */
@Stateless
public class FileEJB {

 
    @PersistenceContext(unitName = "FileManagerPU")
    private EntityManager em;

 
    public File addNewFile(String name, String remark, String path, String originalFilename, String mimeType, Date dateFile, Folder folder) {

        java.io.File f = new java.io.File(path);

        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFile);
        cal.set(Calendar.HOUR, 1);

        File file = new File();
        file.setName(name);
        file.setRemark(remark);
        file.setPath(path);
        file.setFolder(folder);
        file.setFileDate(cal.getTime());
        file.setSize(f.length());
        file.setOriginalFilename(originalFilename);
        file.setType(mimeType);
     
        folder.addFile(file);
        System.out.println("File EJB:"+name);
        em.persist(file);
        em.merge(folder);

        return file;

    }


    public void updateFile(File file) {
        em.merge(file);
    }

  
    public void deleteFile(File file) {
        File toRemoved = em.merge(file);
        em.remove(toRemoved);
    }
    
  
    public String downloadFile(File file){
        return file.getPath();
    }

}
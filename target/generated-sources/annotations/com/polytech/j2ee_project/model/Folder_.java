package com.polytech.j2ee_project.model;

import com.polytech.j2ee_project.model.File;
import com.polytech.j2ee_project.model.Folder;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-05-12T10:47:36")
@StaticMetamodel(Folder.class)
public class Folder_ { 

    public static volatile SingularAttribute<Folder, Date> updateDate;
    public static volatile SingularAttribute<Folder, Folder> parentFolder;
    public static volatile ListAttribute<Folder, Folder> childFolders;
    public static volatile SingularAttribute<Folder, String> name;
    public static volatile SingularAttribute<Folder, Date> insertDate;
    public static volatile ListAttribute<Folder, File> files;
    public static volatile SingularAttribute<Folder, Long> id;

}
package com.polytech.j2ee_project.model;

import com.polytech.j2ee_project.model.Folder;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-05-12T10:47:36")
@StaticMetamodel(File.class)
public class File_ { 

    public static volatile SingularAttribute<File, String> path;
    public static volatile SingularAttribute<File, Date> updateDate;
    public static volatile SingularAttribute<File, Folder> folder;
    public static volatile SingularAttribute<File, Long> size;
    public static volatile SingularAttribute<File, Date> insertDate;
    public static volatile SingularAttribute<File, String> name;
    public static volatile SingularAttribute<File, String> remark;
    public static volatile SingularAttribute<File, Date> fileDate;
    public static volatile SingularAttribute<File, Long> id;
    public static volatile SingularAttribute<File, String> type;
    public static volatile SingularAttribute<File, String> originalFilename;

}